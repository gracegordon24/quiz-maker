ackage finalproject;
import java.util.EmptyStackException;
class ArrayStack {
    private String [] s;  // Holds stack elements
   private int top;   // Stack top pointer

 public ArrayStack (int capacity)
   {
       s = new String[capacity];
       top = 0;
   }
 public boolean empty() 
   { 
       return top == 0; 
   }
  public void push(String x) throws StackOverFlowException 
   {
       if (top == s.length)  
           throw new StackOverFlowException();
       else
       {
          s[top] = x;
          top ++;           
       }         
   }
   public String pop()
   {
       if (empty())
           throw new EmptyStackException();
       else
       {
          top--;
          return s[top];
       }
   }
   
   /** 
      The peek method returns the value at the
      top of the stack.
      @return value at top of the stack.
		@exception EmptyStackException When the 
		stack is empty.
   */
   
   String peek()
   {
       if (empty())
           throw new EmptyStackException();
       else
       {
           return s[top-1];
       }
   }
   String get(int x)
   {
       if (empty())
           throw new EmptyStackException();
       else
       {
           return s[x];
       }
    }
}
