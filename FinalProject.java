package finalproject;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner; 
import java.lang.String;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FinalProject {
       public static void main(String[] args) throws FileNotFoundException, StackOverFlowException, IOException {
// displaying a message to the user of the instructions on how to exit the quiz 
//before entering their information.
           System.out.println("If you would like to exit the quiz before or after, "
                   + "please enter 'done' for both username and password when prompted");
           System.out.println("If you would like to proceed, please enter"
                   + " your username and password as usual");
//called the variables in the main to make sure it will grab the data 
//information that i declared to these strings
         String firstName;
         String lastName;
 //calling in the login method in the main to start the chunk of the code 
 //to prompt the user for username and password and verify the user
           login();
       }
       
//the login process
public static  void login() throws FileNotFoundException, StackOverFlowException, IOException{
//creating a scanner for take in keyboard input
        Scanner userInput = new Scanner(System.in);
//prompts the display message for the user to enter their username
        System.out.print("Enter a username: ");
//the scanner that will take in the user's input for the username
        String userName = userInput.nextLine();
//prompts the display message for the user to enter their password
        System.out.print("Enter password: ");
//another scanner declared to take in user input to enter their password
        String pwd = userInput.nextLine();
        System.out.println("");
//string's declared to be used further in the code for printing out user's informaiton
        String firstName;
        String lastName;
//declaring file input with my computer's pathfile to grab the file and it's data
    File inputFile = new File("C:\\Users\\grace\\OneDrive\\Data Structures SP22"
            + "\\UsersInfo.txt");
//the scanner is then declared to scan the file and read line by line
    Scanner fileInput = new Scanner(inputFile);
//a boolean declared to the login. Right now it's first stated as it being falsed
    boolean login = false;
//a for loop to encounter the amount of attempts the user can only use to verify their login
    for(int attempt = 1; attempt < 3; attempt++){
//a while loop that will continue to read the nextlines of file and declaring since login is false
    while(fileInput.hasNextLine() && login == false) {
//with the while loop running, I have a string array declared that 
//reads next line and cleans up how it's being read
    String [] line = fileInput.nextLine().split("\t");
//a if statement that if the user types in done, it will let the user know 
//they will now exit the quiz 
    if(userName.equalsIgnoreCase("done")|| userName.equalsIgnoreCase("DONE")){
        userName = "DONE";
        System.out.println("You are now exiting the quiz");
        System.exit(0);
    }
//another if statments comparing the input of both the username and password and matching it with the file 
//to find who is taking quiz or if they are even a student
    if(userName.equals(line[2]) && pwd.equals(line[3]) && "Student".equals(line[4])) {
//if what was typed true it will print the message of the user first and last name
        System.out.println("Welcome " + line[0] + " " + line[1]);
        firstName = line[0];
        lastName = line[1];
//in the if statement, have the boolean variable to say this is true and the statement 
//after will execute starting the quiz
        login = true;
//the method of starting the quiz and in the parenthesis are the varibles I am calling in it
        startQuiz(userName, firstName, lastName);
    }
//another if statement to check when the user types in their login info, if it matches instructor, 
//it will execute this statments block of code
            if("Instructor".equals(line[4]) && userName.equals(line[2]) && pwd.equals(line[3])){
            System.out.println("Hi instructor, please complete these tasks: ");
            System.out.print("1. Register a new student \n");
            System.out.print("2. Display stats \n");
            System.out.print("3. Add new Questions \n");
            System.exit(0);
                   
            }
            }
//if login was true with the information they typed, they do not need to 
//prompted again so it says attemp = 3 already
    if(login == true) {
             attempt = 3;
    }
//then if the user's input is not matching to the file it will continue to prompt it again 
//and display the message that their input was invalid
    else{
        System.out.println("Invalid username or password. Please try again.");
        System.out.print("Enter a username: ");
        userName = userInput.nextLine();
        System.out.print("Enter password: ");
        pwd = userInput.nextLine();
        System.out.println("Invalid username or password. Please try again.");
        System.out.print("Enter a username: ");
        userName = userInput.nextLine();
        System.out.print("Enter password: ");
        pwd = userInput.nextLine();
        System.out.println("Too many failed attempts. Closing application");
        System.exit(0);
        }
        }

        }
//the method for the grade/score. Takes in the data information I declared in the startquiz 
//and uses it to plug in the equation for the grade
    public static double gradeAverage(int correct, int total){
      return correct * 100 / total;
    }
//the start quiz block of code for the user to take once passing the login in verification
public static void startQuiz(String userName, String firstName, String lastName) throws FileNotFoundException, StackOverFlowException, IOException {  
//declared integers for the score
        int correct = 0;
        int total = 10;
//int score = correct * 100 / total;
       
//arrayList created to hold in the user's answer when they answered the question 
//and one for the answer's bank to comparison
        ArrayList<String> userAnswer = new ArrayList<>();
        ArrayList<String> answerRecord = new ArrayList<>();
//display's the message for the user how to answer the quiz and that it's 10 quesitons
        System.out.print("This test is 10 questions. Please answer true or false \n");
//the timer started to keep track of how long it took the user to take the test
        long start = System.currentTimeMillis();
//created an array stack for the questions to hold 125 which is how many questions there are in the file
        ArrayStack questions = new ArrayStack(125);
//creating the file and it's path where it's located so it can be read
        File quizFile = new File("C:\\Users\\grace\\OneDrive\\Data Structures SP22"
                    + "\\TestBank.txt");
//the scanner for the file of the questions
        Scanner q = new Scanner(quizFile);
//another array stack created for the correct answers for the user's input being compared to
        ArrayStack answerBank = new ArrayStack (125);
//the file for the answers and the pathname for where it's located
        File answerFile = new File("C:\\Users\\grace\\OneDrive\\Data Structures SP22"
                + "\\Answers.txt");
//the scanner to scan that file to read it line by line
        Scanner a = new Scanner(answerFile);
//a while loop witht the conditions of reading it until it reaches the end of the file's data
        while (q.hasNextLine()&& a.hasNextLine()) {
            questions.push(q.nextLine());
            answerBank.push(a.nextLine());
        }
//declared random for the questions when preparing the quiz
        Random rand = new Random();
//a for loop for how many questions that will print
        for (int i = 0; i < 10; i++) {
//the declared integer for generating a new set of questions
            int qNum = rand.nextInt(124);
//displays the questions and retrieving the information of the questions with taking 
//in the keyboard input of what the user answers
            System.out.println("Question " + (i+1) + ": "+ questions.get(qNum));
            Scanner answer = new Scanner(System.in);
            System.out.print("Answer T or F: ");
            String userAns = answer.nextLine();
//a while loop for answering the questions making sure it's true or false and if not, it keeps prompting until then
            while(!userAns.equalsIgnoreCase("True") && !userAns.equalsIgnoreCase("T") 
            && !userAns.equalsIgnoreCase("False") && !userAns.equalsIgnoreCase("F")){
                System.out.println("Please answer with True/False or T/F");
                userAns = answer.nextLine();
            }
//the two if statements of taking what the user enter's and making sure if it is true or false, it's not case sensitive cause 
//it's either that or T or F
            if(userAns.equalsIgnoreCase("True") || userAns.equalsIgnoreCase("T") 
            || userAns.equalsIgnoreCase("False")|| userAns.equalsIgnoreCase("F")) {
            if(userAns.equalsIgnoreCase("True") || userAns.equalsIgnoreCase("T"))    
            { userAns = "TRUE";
            }
            else if(userAns.equalsIgnoreCase("False") || userAns.equalsIgnoreCase("F")) {
                    userAns = "FALSE";
                    }
//this is taking the user's input and adding it to the arraylist i created above
            userAnswer.add(userAns);
            }
// if the userAnswer's answer is equal to the bank and retrieve those correct answers
            if(userAns.equalsIgnoreCase(answerBank.get(qNum))){
                //everytime answered right it will add point to the score
                correct++;
            }
// with the answers, it grabs the information of which questions and adds to the arraylist i reated above
            answerRecord.add(answerBank.get(qNum));
        }
//after the program finishes it takes the end of the time it took
        long end = System.currentTimeMillis();
        float sec = (end - start) / 10000F;
//prints out all the information of who the user is and the grade as well as the user's answers and the actual correct answers
        System.out.println("\n");
       System.out.println("Frist Name: " + firstName);
       System.out.println("Last Name: " + lastName);
       System.out.println("Grade: " + gradeAverage(correct,total));
       System.out.print(sec + "seconds");
       System.out.println("\n");
       for (int i = 0; i < 10; i++) {
      
      System.out.println("Question " + (i+1) + "\n"+ "Your Answer: " + userAnswer.get(i));
      System.out.println("Correct Answer: " + answerRecord.get(i));
       }
//declaring the varibale for the current time and date for the file to be named
        var today = new Date();
        var time = today.getHours() + "_" + today.getMinutes() + "_" + today.getSeconds();
        String CurrentTime = time;
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("MM_dd_yy");
        String CurrentDate = formatter.format(date);
//after assigning the data to the strings they belong to, I use them in the status reports name
        String statusReport = userName + "_COSC_236_Quiz_"+ CurrentDate + "_" 
                + CurrentTime + ".txt" ; 
//the file I created and where to put it with the information that will be written to it
       File file = new File ("C:\\Users\\grace\\OneDrive\\Data Structures SP22\\" + statusReport);
       FileWriter fw = new FileWriter(file);
       fw.write("Frist Name: " + firstName);
       fw.write("\n");
       fw.write("Last Name: " + lastName);
       fw.write("\n");
       fw.write("Grade: " + gradeAverage(correct,total));
       fw.write("\n");
       fw.write(sec + "seconds");
       fw.write("\n");
       for(int i =0; i< 10; i++) {
       fw.write("\n" + "Question " + (i+1+ "\n") + "Your Answer: " + userAnswer.get(i));
       fw.write("\n");
       fw.write("Correct Answer: " + answerRecord.get(i));

       }
//closes the file writer and displays the message that the report has been made 
       fw.close();
       System.out.println("Your grade report has been made");
     
//calls the restart quiz method for it to prompt the user again for another quiz if they want to
        restartQuiz();
        }
       
        
public static void restartQuiz() throws FileNotFoundException, StackOverFlowException, IOException {
//goes back to the login method to reprompt the user. 
System.out.println("");
    login();
}

}
